import React, { useState } from 'react';
import { Container, Typography, ButtonGroup, Button, Table, TableBody, TableCell, TableRow, Paper, Grid } from '@mui/material';

function App() {
  const [valg, setValg] = useState('');
  const [dager, setDager] = useState(''); // Ingen dager valgt som standard
  const gratisKjernetid = (valg === 'ettermiddag' || valg === 'helDag') && dager ? 1400 : 0; // Gratis kjernetid beregnes kun hvis dager og type er valgt
  const priser = {
    morgen: 223,
    ettermiddag: 522,
    helDag: 744,
  };
  const kostpengerPrUke = [0, 77, 152, 228, 304, 379];

  // Beregn oppholdspris kun hvis både valg og dager er satt
  const oppholdPris = valg && dager ? priser[valg] * parseInt(dager, 10) : 0;
  // Beregn kostpenger basert på antall dager valgt
  const kostpenger = dager ? kostpengerPrUke[parseInt(dager, 10)] : 0;
  const total = oppholdPris + kostpenger - gratisKjernetid;

  const dagerOptions = [1, 2, 3, 4, 5];

  return (
    <Container maxWidth="md" sx={{ mt: 4 }}>
      <Typography variant="h4" gutterBottom>SFO Priskalkulator</Typography>
      <Typography variant="subtitle1" sx={{ mb: 3 }}>
        Bruk denne praktiske kalkulatoren for å estimere månedlige kostnader for skolefritidsordning (SFO) basert på antall dager og type opphold. Kalkulatoren vil også vise hvordan gratis kjernetid påvirker den totale kostnaden.
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Paper elevation={3} sx={{ p: 2, height: '100%' }}>
            <Typography variant="h6" gutterBottom>SFO-tid</Typography>
            <ButtonGroup fullWidth variant="contained" aria-label="outlined primary button group" sx={{ mb: 2 }}>
              {Object.keys(priser).map((key) => (
                <Button
                  key={key}
                  color={valg === key ? 'primary' : 'inherit'}
                  onClick={() => setValg(key)}
                  sx={{ flexGrow: 1 }}>
                  {key.charAt(0).toUpperCase() + key.slice(1)}
                </Button>
              ))}
            </ButtonGroup>
            <Typography variant="h6" gutterBottom>Antall dager</Typography>
            <ButtonGroup fullWidth variant="contained" aria-label="outlined primary button group">
              {dagerOptions.map((dag) => (
                <Button
                  key={dag}
                  color={dager === dag.toString() ? 'primary' : 'inherit'}
                  onClick={() => setDager(dag.toString())}
                  sx={{ flexGrow: 1 }}>
                  {dag}
                </Button>
              ))}
            </ButtonGroup>
          </Paper>
        </Grid>
        <Grid item xs={12} md={6}>
          <Paper elevation={3} sx={{ p: 2, height: '100%' }}>
            <Typography variant="h6" gutterBottom>Oppsummering</Typography>
            <Table size="small">
              <TableBody>
                <TableRow>
                  <TableCell>Oppholdspris</TableCell>
                  <TableCell align="right">{oppholdPris} kr</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Gratis kjernetid (12 timer per uke)</TableCell>
                  <TableCell align="right">-{gratisKjernetid} kr</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Kostpenger</TableCell>
                  <TableCell align="right">{kostpenger} kr</TableCell>
                </TableRow>
                <TableRow sx={{ fontWeight: 'bold' }}>
                  <TableCell><b>Totalt å betale</b></TableCell>
                  <TableCell align="right"><b>{total} kr</b></TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
